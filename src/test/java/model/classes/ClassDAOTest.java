package model.classes;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ClassDAOTest {

    private ClassDAO classDAOtest = new ClassDAO();

    @Test
    public void createClass() {
        classDAOtest.createClass();

        Assert.assertTrue(classDAOtest.getClassByID(1).isPresent());
        Assert.assertFalse(classDAOtest.getClassByID(2).isPresent());
        Assert.assertEquals(classDAOtest.classes.size(),1);

    }

    @Test
    public void addStudentToTheClass() {
        classDAOtest.addStudentToTheClass(1,1);
        classDAOtest.addStudentToTheClass(1,2);

        Assert.assertEquals(classDAOtest.classToStudents.size(),2);
        Assert.assertTrue(classDAOtest.getClassToStudentById(1).isPresent());
        Assert.assertTrue(classDAOtest.getClassToStudentById(2).isPresent());
        Assert.assertFalse(classDAOtest.getClassToStudentById(3).isPresent());
    }

    @Test
    public void removeStudentFromClass() {
        classDAOtest.addStudentToTheClass(1,1);
        classDAOtest.addStudentToTheClass(1,2);
        classDAOtest.addStudentToTheClass(1,3);
        Assert.assertEquals(classDAOtest.classToStudents.size(),3);

        classDAOtest.removeStudentFromClassToStudents(1);
        Assert.assertEquals(classDAOtest.classToStudents.size(), 2);
    }
}