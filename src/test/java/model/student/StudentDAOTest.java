package model.student;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

class StudentDAOTest {

    StudentDAO test = new StudentDAO();

    @Test
    void checkIfStudentAddedAndListSize() {
        test.addStudent();
        test.addStudent();

        Assert.assertEquals(test.getStudents().size(), 2);

        Assert.assertTrue(test.getStudentById(1).isPresent());
        Assert.assertFalse(test.getStudentById(4).isPresent());
    }

    @Test
    void removeStudentOnId() {
        test.addStudent();
        test.addStudent();
        test.removeStudentOnId(2);
        test.addStudent();

        Assert.assertTrue(test.getStudentById(2).isPresent());
        Assert.assertFalse(test.getStudentById(3).isPresent());
    }
}