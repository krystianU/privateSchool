package model.grade;

import org.junit.Assert;
import org.junit.Test;

public class GradeDAOTest {

    private GradeDAO gradeDAOtest = new GradeDAO();

    @Test
    public void addGradeToStudent() {
        gradeDAOtest.addGradeToStudent(5, 1);
        gradeDAOtest.addGradeToStudent(1, 1);

        Assert.assertTrue(gradeDAOtest.getGradeById(1).isPresent());
        Assert.assertTrue(gradeDAOtest.getGradeById(2).isPresent());
        Assert.assertFalse(gradeDAOtest.getGradeById(5).isPresent());
        Assert.assertEquals(gradeDAOtest.getGradeById(1).get().grade, 5);
        Assert.assertEquals(gradeDAOtest.getGradeById(2).get().grade, 1);
        Assert.assertEquals(gradeDAOtest.getGradeById(2).get().hasBeenChanged, false);
        Assert.assertEquals(gradeDAOtest.getGradeById(1).get().subjectToStudentId, 1);
    }

    @Test
    public void countAverageForSubjectAndStudent() {

        gradeDAOtest.addGradeToStudent(5, 1);
        gradeDAOtest.addGradeToStudent(1, 1);
        Assert.assertEquals(gradeDAOtest.countAverageForSubjectAndStudent(1), 3, 2);
    }

    @Test(expected = ArithmeticException.class)
    public void checkIfAverageThrowsException() {
        Assert.assertEquals(gradeDAOtest.countAverageForSubjectAndStudent(2), 5, 0);
    }

    @Test
    public void changeGradeForStudent() {

        gradeDAOtest.addGradeToStudent(5, 1);
        gradeDAOtest.addGradeToStudent(1, 1);

        Assert.assertEquals(gradeDAOtest.getGradeById(2).get().grade, 1);

        gradeDAOtest.changeGradeForStudent(5, 2);

        Assert.assertEquals(gradeDAOtest.getGradeById(2).get().grade, 5);
        Assert.assertEquals(gradeDAOtest.getGradeById(2).get().hasBeenChanged, true);

    }
    @Test(expected = ArithmeticException.class)
    public void checkIfThrowsExceptionNoGrades(){
        gradeDAOtest.countAverageForSubjectAndStudent(1);
    }

    @Test
    public void checkIfAllGradesPass() {
        gradeDAOtest.addGradeToStudent(1, 1);
        Assert.assertFalse(gradeDAOtest.checkIfAllGradesPass());
    }

    @Test
    public void getGradesListForStudent() {

    }

    @Test
    public void removeGradesFromStudent() {

        gradeDAOtest.addGradeToStudent(1,1);
        gradeDAOtest.addGradeToStudent(2,1);
        gradeDAOtest.addGradeToStudent(3,1);
        gradeDAOtest.removeGradesFromStudent(1);
        Assert.assertEquals(0,gradeDAOtest.grades.size());
    }
}