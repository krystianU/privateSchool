package model.subject;

import org.junit.Assert;
import org.junit.jupiter.api.Test;

import java.util.Optional;

class SubjectDAOTest {

    SubjectDAO subjectDAOtest = new SubjectDAO();

    @Test
    void addSubject() {
        subjectDAOtest.addSubject();
        subjectDAOtest.addSubject();
        Assert.assertTrue(subjectDAOtest.getSubjectById(1).isPresent());
        Assert.assertTrue(subjectDAOtest.getSubjectById(2).isPresent());
        Assert.assertFalse(subjectDAOtest.getSubjectById(3).isPresent());

    }

    @Test
    void addStudentToSubject() {
        subjectDAOtest.addStudentToSubject(1, 1);
        subjectDAOtest.addStudentToSubject(1, 2);

        Assert.assertEquals(subjectDAOtest.getSubjectToStudentList().size(), 2);
        Assert.assertNotEquals(subjectDAOtest.getSubjectToStudentList().size(), 3);

        Assert.assertTrue(subjectDAOtest.getSubjectToStudentById(1).isPresent());
        Assert.assertFalse(subjectDAOtest.getSubjectToStudentById(3).isPresent());

    }

    @Test
    void getSubjectListForStudent() {
        subjectDAOtest.addStudentToSubject(1, 1);
        subjectDAOtest.addStudentToSubject(2, 2);

        Assert.assertEquals(subjectDAOtest.getSubjectListForStudent(1).size(), 1);
        Assert.assertNotEquals(subjectDAOtest.getSubjectListForStudent(2).size(), 2);
    }

    @Test
    void removeSubjectsFromStudent() {
        subjectDAOtest.addStudentToSubject(1, 1);
        subjectDAOtest.addStudentToSubject(2, 1);
        subjectDAOtest.addStudentToSubject(3, 1);
        subjectDAOtest.addStudentToSubject(3, 2);

        Assert.assertEquals(subjectDAOtest.getSubjectListForStudent(1).size(), 3);

        subjectDAOtest.removeSubjectsFromStudent(1);

        Assert.assertEquals(subjectDAOtest.getSubjectListForStudent(1).size(), 0);

    }

    @Test
    void getSubjectById(){
        subjectDAOtest.addSubject();

        Assert.assertTrue(subjectDAOtest.getSubjectById(1).isPresent());
        Assert.assertFalse(subjectDAOtest.getSubjectById(2).isPresent());
    }

    @Test
    void getSubjectToStudentById(){
        subjectDAOtest.addStudentToSubject(1,1);
        Assert.assertTrue(subjectDAOtest.getSubjectToStudentById(1).isPresent());
        Assert.assertFalse(subjectDAOtest.getSubjectToStudentById(2).isPresent());
    }

}