package view;

import controller.ConsoleController;
import controller.ScannerController;
import controller.SchoolController;
import model.database.Database;

public class School implements SchoolMVC.View {

    private ConsoleMVC.Controller consoleController = new ConsoleController();
    private ScannerMVC.Controller scannerController = new ScannerController();
    private SchoolMVC.Controller schoolController = new SchoolController(Database.getInstance());

    @Override
    public void showMenuOptions() {
        consoleController.println("1. Student management");
        consoleController.println("2. Subject management");
        consoleController.println("3. Class management");
    }

    @Override
    public void menuStudent() {
        consoleController.println("1. Create student");
        consoleController.println("2. Remove student");
        consoleController.println("3. Assign student to a class");
        consoleController.println("4. Assign subjects to a student");
        consoleController.println("0. Return");

    }

    @Override
    public void menuClass() {
        consoleController.println("1. Create class");
        consoleController.println("2. Assign students to a class");
        consoleController.println("0. Return");
    }

    @Override
    public void menuGrade() {
        consoleController.println("1. Assign grade to a student");
        consoleController.println("2. Correct student's grade");
        consoleController.println("0. Return");

    }

    @Override
    public void menuSubject() {
        consoleController.println("1. Create subject");
        consoleController.println("2. Assign student to a subject");
        consoleController.println("0. Return");
    }

    @Override
    public void startProgram() {
        schoolController.attachView(this);

    }
}
