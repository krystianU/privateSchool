package view;

public interface ConsoleMVC {

    interface Controller{
        void println(String string);
    }
}
