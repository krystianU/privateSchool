package view;

public interface SchoolMVC {

    interface Controller{
        void attachView(View view);
        void startSchoolYear();
        void finishSchoolYear();
        void createStudent();
        void createSubject();
        void createClass();
        void addGradeToStudent();
        void pickOptionFromMenu();
        void mainMenu();
        void menuStudent();
        void menuClass();
        void menuGrade();
        void menuSubject();
        void removeStudent();
        void assignStudentToClass();
        void assignSubjectsToStudent();
        void assignGradeToStudent();
        void correctStudentGrade();


    }

    interface View{
        void showMenuOptions();
        void menuStudent();
        void menuClass();
        void menuGrade();
        void menuSubject();
        void startProgram();

    }
}
