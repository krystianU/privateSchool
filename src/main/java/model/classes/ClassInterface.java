package model.classes;

import java.util.List;
import java.util.Optional;

public interface ClassInterface {

    void createClass();
    void addStudentToTheClass(int classId, int studentId);
    void removeStudentFromClassToStudents(int studentId);
    public Optional<Classes> getClassByID(int id);
    public List<ClassToStudent> getClassToStudents();
    public Optional<ClassToStudent> getClassToStudentById(int id);

}
