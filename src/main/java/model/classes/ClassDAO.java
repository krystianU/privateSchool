package model.classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClassDAO implements ClassInterface {

    List<Classes> classes = new ArrayList<>();
    List<ClassToStudent> classToStudents = new ArrayList<>();

    @Override
    public void createClass() {
        Classes class1 = new Classes();
        if (classes.isEmpty()) {
            class1.id = 1;
        } else {
            class1.id = classes.get(classes.size() - 1).id + 1;
        }
        classes.add(class1);
    }

    @Override
    public void addStudentToTheClass(int classId, int studentId) {
        ClassToStudent classToStudent = new ClassToStudent();
        if (classToStudents.isEmpty()) {
            classToStudent.id = 1;
        } else {
            classToStudent.id = classToStudents.get(classToStudents.size() - 1).id + 1;
        }
        if (classToStudents.stream().noneMatch(classToStudent1 -> classToStudent.studentId == studentId)) {
            classToStudent.studentId = studentId;
            classToStudent.classId = classId;
            classToStudents.add(classToStudent);
        } else {
            System.out.println("Student already assigned to a class!");
        }
    }

    @Override
    public void removeStudentFromClassToStudents(int studentId) {
        classToStudents = classToStudents.stream()
                .filter(classToStudent -> classToStudent.studentId != studentId).collect(Collectors.toList());
    }

    @Override
    public Optional<Classes> getClassByID(int id) {
        return classes.stream()
                .filter(classes -> classes.id == id)
                .findFirst();
    }

    @Override
    public List<ClassToStudent> getClassToStudents() {
        return classToStudents;
    }

    @Override
    public Optional<ClassToStudent> getClassToStudentById(int id) {
        return classToStudents.stream()
                .filter(classToStudent -> classToStudent.id == id)
                .findFirst();
    }
}
