package model.database;

import model.classes.ClassDAO;
import model.grade.GradeDAO;
import model.student.StudentDAO;
import model.subject.SubjectDAO;

public interface DatabaseInterface {
    void calculateAverageForAllSubjects();
    void checkIfAverageTargetIsMet();
    void removeStudentFromSchool(int studentId);
    StudentDAO getStudentDao();
    GradeDAO getGradeDao();
    SubjectDAO getSubjectDao();
    ClassDAO getClassDao();
}
