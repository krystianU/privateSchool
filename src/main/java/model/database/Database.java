package model.database;

import model.classes.ClassDAO;
import model.grade.GradeDAO;
import model.student.StudentDAO;
import model.subject.SubjectDAO;

import java.util.ArrayList;
import java.util.List;

public class Database implements DatabaseInterface {

    private static Database instance;
    private GradeDAO gradeDAO;
    private StudentDAO studentDAO;
    private SubjectDAO subjectDAO;
    private ClassDAO classDAO;

    public Database() {

    }

    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();

            instance.subjectDAO = new SubjectDAO();
            instance.studentDAO = new StudentDAO();
            instance.gradeDAO = new GradeDAO();
            instance.classDAO = new ClassDAO();
        }
        return instance;
    }

    @Override
    public void calculateAverageForAllSubjects() {
        subjectDAO.getSubjectToStudentList()
                .forEach(subjectToStudent -> subjectToStudent
                        .average = gradeDAO.countAverageForSubjectAndStudent(subjectToStudent.id));
    }

    @Override
    public void checkIfAverageTargetIsMet() {
        List<Integer> studentsToRemove = new ArrayList();
        studentDAO.getStudents().forEach(student -> {
            double average = gradeDAO.countAverageForSubjectAndStudent(student.id);
            if (average < 4.3) {
                studentsToRemove.add(student.id);
                removeStudentFromSchool(student.id);
            }
        });
        for (Integer idToRemove : studentsToRemove) {
            studentDAO.removeStudentOnId(idToRemove);
        }
    }

    @Override
    public void removeStudentFromSchool(int studentId) {
        subjectDAO.getSubjectListForStudent(studentId)
                .forEach(subjectToStudent -> {
                    gradeDAO.removeGradesFromStudent(subjectToStudent.id);
                });
        subjectDAO.removeSubjectsFromStudent(studentId);
        classDAO.removeStudentFromClassToStudents(studentId);
    }

    @Override
    public StudentDAO getStudentDao() {
        return studentDAO;
    }

    @Override
    public GradeDAO getGradeDao() {
        return gradeDAO;
    }

    @Override
    public SubjectDAO getSubjectDao() {
        return subjectDAO;
    }

    @Override
    public ClassDAO getClassDao() {
        return classDAO;
    }


}
