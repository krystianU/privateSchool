package model.grade;

public class Grade {

    public int id;
    public int subjectToStudentId;
    public int grade;
    public boolean hasBeenChanged;

}
