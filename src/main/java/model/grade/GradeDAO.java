package model.grade;

import model.gradeToStudent.GradeListener;
import model.subjectToStudent.SubjectToStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class GradeDAO implements GradeInterface {

    List<Grade> grades = new ArrayList<>();
    List<Grade> averageGrade = new ArrayList<>();


    @Override
    public void addGradeToStudent(int grade, int subjectToStudentId) {
        Grade grade1 = new Grade();
        if (grades.isEmpty()) {
            grade1.id = 1;
        } else {
            grade1.id = grades.get(grades.size() - 1).id + 1;
        }
        grade1.subjectToStudentId = subjectToStudentId;
        grade1.grade = grade;
        grades.add(grade1);
    }

    @Override
    public double countAverageForSubjectAndStudent(int subjectToStudentId) {
        double average;
        int sum = 0;
        List<Grade> tempList = grades.stream()
                .filter(grade -> subjectToStudentId == grade.subjectToStudentId).collect(Collectors.toList());
        if (tempList.size() != 0) {
            for (Grade grade : tempList) {
                sum += grade.grade;
            }
            average = sum / tempList.size();
            return average;
        } else {
            throw new ArithmeticException("No grades assigned to this student.");
        }
    }

    @Override
    public void changeGradeForStudent(int newGrade, int gradeId) {

        grades.stream().filter(grade -> gradeId == grade.id).forEach(grade -> {
            if (grade.hasBeenChanged != true) {
                grade.grade = newGrade;
                grade.hasBeenChanged = true;
            } else {
                System.out.println("You can't change the same grade more than once.");
            }
        });
    }

    @Override
    public boolean checkIfAllGradesPass() {
        if (grades.stream().anyMatch(grade -> grade.grade == 1)) {
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void getGradesListForStudent(List<SubjectToStudent> subjectToStudents, GradeListener gradeListener) {

    }

    @Override
    public void removeGradesFromStudent(int subjectToStudentId) {
        grades = grades
                .stream()
                .filter(grade -> grade.subjectToStudentId != subjectToStudentId)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Grade> getGradeById(int id) {
        return grades.stream()
                .filter(grade -> grade.id == id)
                .findFirst();
    }

    @Override
    public List<Grade> getGrades() {
        return grades;
    }
}
