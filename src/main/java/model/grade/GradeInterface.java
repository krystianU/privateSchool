package model.grade;

import model.gradeToStudent.GradeListener;
import model.subjectToStudent.SubjectToStudent;

import java.util.List;
import java.util.Optional;

public interface GradeInterface {

    void addGradeToStudent(int grade, int subjectToStudentId);
    double countAverageForSubjectAndStudent(int subjectToStudentId);
    void changeGradeForStudent(int newGrade, int gradeId);
    boolean checkIfAllGradesPass();
    void getGradesListForStudent(List<SubjectToStudent> subjectToStudents, GradeListener gradeListener);
    void removeGradesFromStudent(int subjectToStudentId);
    Optional<Grade> getGradeById(int id);
    List<Grade> getGrades();

}
