package model.subjectToStudent;

import model.subject.Subject;

import java.util.List;

public interface SubjectListener {

    void getSubjects(List<Subject> subjects);
}
