package model.subject;

import model.subjectToStudent.SubjectToStudent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class SubjectDAO implements SubjectDAOInterface {

    private List<Subject> subjects = new ArrayList<>();
    private List<SubjectToStudent> subjectToStudentList = new ArrayList<>();

    @Override
    public void addSubject() {
        Subject subject = new Subject();
        if (subjects.isEmpty()) {
            subject.id = 1;
        } else {
            subject.id = subjects.get(subjects.size() - 1).id + 1;
        }
        subjects.add(subject);
    }

    @Override
    public void addStudentToSubject(int subjectId, int studentId) {
        SubjectToStudent subjectToStudent = new SubjectToStudent();
        if (subjectToStudentList.isEmpty()) {
            subjectToStudent.id = 1;
        } else {
            subjectToStudent.id = subjectToStudentList.get(subjectToStudentList.size() - 1).id + 1;
        }
        subjectToStudent.studentId = studentId;
        subjectToStudent.subjectId = subjectId;
        subjectToStudentList.add(subjectToStudent);
    }

    @Override
    public List<SubjectToStudent> getSubjectListForStudent(int studentId) {
        return subjectToStudentList.stream()
                .filter(subjectToStudent -> subjectToStudent.studentId == studentId)
                .collect(Collectors.toList());

    }

    @Override
    public void removeSubjectsFromStudent(int studentId) {
        subjectToStudentList = subjectToStudentList
                .stream()
                .filter(subjectToStudent -> subjectToStudent.studentId != studentId)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<Subject> getSubjectById(int id) {
        return subjects.stream()
                .filter(subject -> subject.id == id)
                .findFirst();
    }

    @Override
    public Optional<SubjectToStudent> getSubjectToStudentById(int id) {
        return subjectToStudentList.stream()
                .filter(subjectToStudent -> subjectToStudent.id == id)
                .findFirst();
    }

    @Override
    public List<Subject> getSubjectsList() {
        return subjects;
    }

    @Override
    public List<SubjectToStudent> getSubjectToStudentList() {
        return subjectToStudentList;
    }


}
