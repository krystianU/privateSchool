package model.subject;

import model.subjectToStudent.SubjectToStudent;

import java.util.List;
import java.util.Optional;

public interface SubjectDAOInterface {

    void addSubject();

    void addStudentToSubject(int subjectId, int studentId);

    List<SubjectToStudent> getSubjectListForStudent(int studentId);

    void removeSubjectsFromStudent(int studentId);

    public Optional<Subject> getSubjectById(int id);

    public Optional<SubjectToStudent> getSubjectToStudentById(int id);

    public List<Subject> getSubjectsList();

    public List<SubjectToStudent> getSubjectToStudentList();
}
