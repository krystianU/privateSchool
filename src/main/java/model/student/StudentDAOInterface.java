package model.student;

import java.util.Optional;

public interface StudentDAOInterface {
    void addStudent();

    void removeStudentOnId(int id);

    Optional<Student> getStudentById(int id);
}
