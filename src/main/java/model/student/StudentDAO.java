package model.student;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class StudentDAO implements StudentDAOInterface {

    List<Student> students = new ArrayList<>();

    @Override
    public void addStudent() {
        Student student = new Student();
        if (students.isEmpty()) {
            student.id = 1;
        } else {
            student.id = students.get(students.size() - 1).id + 1;
        }
        students.add(student);
    }

    @Override
    public void removeStudentOnId(int id) {
        students = students.stream().filter(student -> student.id != id).collect(Collectors.toList());
    }

    public List<Student> getStudents() {
        return students;
    }

    @Override
    public Optional<Student> getStudentById(int id){
        return students.stream().filter(student -> student.id == id).findFirst();
    }
}
