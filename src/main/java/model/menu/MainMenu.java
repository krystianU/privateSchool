package model.menu;

import controller.ConsoleController;

public class MainMenu {

    private ConsoleController consoleController = new ConsoleController();

    public void showMenuOptions() {
        consoleController.println("1. Start school year");
        consoleController.println("2. Class management");
        consoleController.println("3. Close school year");
    }

}
