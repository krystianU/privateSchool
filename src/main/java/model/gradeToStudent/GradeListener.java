package model.gradeToStudent;

import model.grade.Grade;

import java.util.List;

public interface GradeListener {

    void getGrades(List<Grade> grades);
}
