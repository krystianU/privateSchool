import view.School;
import view.SchoolMVC;

public class Main {

    public static void main(String[] args) {
        SchoolMVC.View schoolView = new School();
        schoolView.startProgram();

    }
}
