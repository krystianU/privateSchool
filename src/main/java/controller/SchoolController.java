package controller;

import model.database.DatabaseInterface;
import view.ConsoleMVC;
import view.ScannerMVC;
import view.SchoolMVC;

import java.util.stream.Collectors;

public class SchoolController implements SchoolMVC.Controller {

    DatabaseInterface database;
    private ScannerMVC.Controller scannerController = new ScannerController();
    private ConsoleMVC.Controller consoleController = new ConsoleController();
    private SchoolMVC.View schoolView;
    private int userInput;


    public SchoolController(DatabaseInterface database) {
        this.database = database;
    }

    @Override
    public void mainMenu() {
        schoolView.showMenuOptions();
        userInput = scannerController.userInputInt();
        switch (userInput) {
            case 1:
                menuStudent();
                break;
            case 2:
                menuSubject();
                break;
            case 3:
                menuClass();
                break;
            default:
                consoleController.println("Invalid option, try again");
                mainMenu();
        }
    }

    @Override
    public void pickOptionFromMenu() {


    }

    @Override
    public void menuStudent() {
        schoolView.menuStudent();
        userInput = scannerController.userInputInt();
        switch (userInput) {
            case 0:
                mainMenu();
                break;
            case 1:
                createStudent();
                break;
            case 2:
                removeStudent();
                break;
            case 3:
                assignStudentToClass();
                break;
            case 4:
                assignSubjectsToStudent();
                break;
            default:
                consoleController.println("Invalid option, try again");
                menuStudent();

        }
    }

    @Override
    public void menuClass() {
        schoolView.menuClass();
        userInput = scannerController.userInputInt();
        switch (userInput) {
            case 0:
                mainMenu();
                break;
            case 1:
                createClass();
                break;
            case 2:
                assignStudentToClass();
                break;
            default:
                consoleController.println("Invalid option, try again");
                menuClass();
        }
    }

    @Override
    public void menuGrade() {
        schoolView.menuGrade();
        userInput = scannerController.userInputInt();
        switch (userInput) {
            case 0:
                mainMenu();
                break;
            case 1:
                assignGradeToStudent();
                break;
            case 2:
                correctStudentGrade();
                break;
            default:
                consoleController.println("Invalid option, try again");
                menuGrade();
        }
    }

    @Override
    public void menuSubject() {
        schoolView.menuSubject();
        userInput = scannerController.userInputInt();
        switch (userInput) {
            case 0:
                mainMenu();
                break;
            case 1:
                createSubject();
                break;
            case 2:
                assignSubjectsToStudent();
                break;
            default:
                consoleController.println("Invalid option, try again");
                menuSubject();
        }
    }

    @Override
    public void removeStudent() {
        consoleController.println("Type student ID: ");
        userInput = scannerController.userInputInt();
        if (!database.getStudentDao().getStudents().stream()
                .filter(student -> student.id == userInput)
                .collect(Collectors.toList()).isEmpty()) {
            database.removeStudentFromSchool(userInput);
            consoleController.println("Student with ID " + userInput + " has been removed.");
        } else {
            consoleController.println("There is no student with such ID!");
        }
        menuStudent();
    }


    @Override
    public void assignStudentToClass() {

    }

    @Override
    public void assignSubjectsToStudent() {

    }

    @Override
    public void assignGradeToStudent() {

    }

    @Override
    public void correctStudentGrade() {

    }


    @Override
    public void attachView(SchoolMVC.View view) {
        this.schoolView = view;
        mainMenu();
    }

    @Override
    public void startSchoolYear() {

    }

    @Override
    public void finishSchoolYear() {

    }

    @Override
    public void createStudent() {
        database.getStudentDao().addStudent();
        consoleController.println("Student has been created.\n");
        menuStudent();
    }

    @Override
    public void createSubject() {
        database.getSubjectDao().addSubject();
        consoleController.println("Subject has been created.\n");
        menuSubject();
    }

    @Override
    public void createClass() {
        database.getClassDao().createClass();
        consoleController.println("Class has been created\n");
        menuClass();
    }

    @Override
    public void addGradeToStudent() {

    }


}
