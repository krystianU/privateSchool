package controller;

import view.ConsoleMVC;

public class    ConsoleController implements ConsoleMVC.Controller {

    @Override
    public void println(String string) {
        System.out.println(string);
    }
}
